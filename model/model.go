package model

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"log"
)

func Paginate(page int, size int) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		offset := page * size
		return db.Offset(offset).Limit(size)
	}
}

type Page struct {
	Size          int
	Number        int
	TotalPages    int
	TotalElements int
	Elements      int
	Content       interface{}
}

type Author struct {
	ID        uint   `json:"id"`
	FirstName string `json:"firstname"`
	LastName  string `json:"lastname"`
}

type Book struct {
	ID          uint   `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
}

var Db *gorm.DB
var err error

func init() {

	Db, err = gorm.Open("sqlite3", "./gorm.db")
	if err != nil {
		log.Fatal("Connection error", err)
	}

	Db.AutoMigrate(&Book{})
	Db.AutoMigrate(&Author{})

}
