package bookHandlers

import (
	"github.com/AndreaNicola/book-api/model"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func List(c *gin.Context) {

	params := c.Request.URL.Query()

	page, errPage := strconv.Atoi(params.Get("page"))
	if errPage != nil {
		page = 0
	}

	size, errSize := strconv.Atoi(params.Get("size"))
	if errSize != nil {
		size = 10
	}

	title := params.Get("title")

	var books []model.Book
	var totalElements int

	countDb := model.Db.Model(&model.Book{})
	if title != "" {
		countDb = countDb.Where("title LIKE %?%", title)
	}

	countDb.Count(&totalElements)

	db := model.Db.Scopes(model.Paginate(page, size))
	db.Find(&books)

	result := model.Page{
		Size:          size,
		Number:        page,
		TotalPages:    totalElements / size,
		TotalElements: totalElements,
		Elements:      len(books),
		Content:       books,
	}

	c.JSON(http.StatusOK, result)
}

func Get(c *gin.Context) {
	id := c.Params.ByName("id")
	var book model.Book

	err := model.Db.Where("id = ?", id).First(&book).Error
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, book)
	}
}

func Delete(c *gin.Context) {
	id := c.Params.ByName("id")
	var book model.Book
	model.Db.Where("id = ?", id).Delete(&book)
	c.Status(http.StatusAccepted)
}

func Create(c *gin.Context) {

	var book model.Book
	err := c.BindJSON(&book)
	if err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
	}

	model.Db.Create(&book)
	c.JSON(http.StatusCreated, book)

}
