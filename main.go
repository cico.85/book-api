package main

import (
	"github.com/AndreaNicola/book-api/authorsHandlers"
	"github.com/AndreaNicola/book-api/bookHandlers"
	"github.com/gin-gonic/gin"
	"log"
)

func main() {

	r := gin.Default()

	{
		bookRouter := r.Group("/books")
		bookRouter.GET("", bookHandlers.List)
		bookRouter.POST("", bookHandlers.Create)
		bookRouter.GET(":id", bookHandlers.Get)
		bookRouter.DELETE(":id", bookHandlers.Delete)
	}

	{
		authorsRouter := r.Group("/authors")
		authorsRouter.GET("/", authorsHandlers.List)
		authorsRouter.POST("/", authorsHandlers.Create)
	}

	errGin := r.Run()
	if errGin != nil {
		log.Fatal("fatal error", errGin)
	}

}
